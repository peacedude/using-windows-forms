﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class tbTempf : Form
    {
        public tbTempf()
        {
            InitializeComponent();
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            
            string inputTemp, outputTemp;
            inputTemp = tempF.Text;

            
            //code to prevent calculation if no value is present in tempF

            if (tempF.Text == String.Empty)
            {
                 MessageBox.Show("ERROR no value entered", "please enter a valid  temperature", MessageBoxButtons.OK, MessageBoxIcon.Error);return; 
            }
            double temp;


            if (ft.Checked) {
                //code for converting Fahrenheit to Celsius



                temp = Convert.ToDouble(inputTemp) - 32.0;
                temp = temp / 9;
                temp = temp * 5;

                //convert back to string

                temp = Math.Round(temp, 2);
                outputTemp = Convert.ToString(temp);
                outputTemp = outputTemp + "c";
            }
            else if(cs.Checked)
            {
                temp = Convert.ToDouble(inputTemp) /5;
                temp = temp * 9;
                temp = temp + 32;
                outputTemp = Convert.ToString(temp);
                outputTemp = outputTemp + "f";
            }
            else
            {
                MessageBox.Show("please select a temperature unit", "no temperature unit selected!", MessageBoxButtons.OK, MessageBoxIcon.Error); return;
            }
               
          
            
            

        }

       
    }
}
