﻿namespace WindowsFormsApplication1
{
    partial class tbTempf
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tempF = new System.Windows.Forms.TextBox();
            this.btnConvert = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ft = new System.Windows.Forms.RadioButton();
            this.cs = new System.Windows.Forms.RadioButton();
            this.result = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tempF
            // 
            this.tempF.Location = new System.Drawing.Point(97, 45);
            this.tempF.Name = "tempF";
            this.tempF.Size = new System.Drawing.Size(100, 20);
            this.tempF.TabIndex = 0;
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(89, 137);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(75, 23);
            this.btnConvert.TabIndex = 2;
            this.btnConvert.Text = "convert";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Temperature";
            // 
            // ft
            // 
            this.ft.AutoSize = true;
            this.ft.Location = new System.Drawing.Point(97, 72);
            this.ft.Name = "ft";
            this.ft.Size = new System.Drawing.Size(75, 17);
            this.ft.TabIndex = 5;
            this.ft.TabStop = true;
            this.ft.Text = "Fahrenheit";
            this.ft.UseVisualStyleBackColor = true;
            // 
            // cs
            // 
            this.cs.AutoSize = true;
            this.cs.Location = new System.Drawing.Point(97, 90);
            this.cs.Name = "cs";
            this.cs.Size = new System.Drawing.Size(57, 17);
            this.cs.TabIndex = 6;
            this.cs.TabStop = true;
            this.cs.Text = "celsius";
            this.cs.UseVisualStyleBackColor = true;
            // 
            // result
            // 
            this.result.AutoSize = true;
            this.result.Location = new System.Drawing.Point(97, 189);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(0, 13);
            this.result.TabIndex = 7;
            // 
            // tbTempf
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.result);
            this.Controls.Add(this.cs);
            this.Controls.Add(this.ft);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnConvert);
            this.Controls.Add(this.tempF);
            this.Name = "tbTempf";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tempF;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton ft;
        private System.Windows.Forms.RadioButton cs;
        private System.Windows.Forms.Label result;
    }
}

